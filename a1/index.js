// https://jsonplaceholder.typicode.com/todos
// 1
fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => {
    let listOfTitles = data.map((todo) => todo.title)
    console.log(listOfTitles)
  })

// 2
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then((response) => response.json())
  .then((data) => console.log(data))

// 2v2
fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then((response) => response.json())
  .then((data) => {
    console.log(
      `Title: ${data.title}
Completed: ${data.completed}`
    )
  })

// 3
fetch('https://jsonplaceholder.typicode.com/todos/', {
  method: 'POST',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'Created a To-do List Item',
    completed: false,
    userId: 1,
    id: 201,
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data))

// 4
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    title: 'Updated To Do List Item',
    description: 'To update the my to do list with a different data strucutre',
    status: 'Pending',
    dateCompleted: 'Pending',
    userId: 1,
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data))

// 5
fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: { 'Content-Type': 'application/json' },
  body: JSON.stringify({
    status: 'Complete',
    dateCompleted: 'February 22, 2023',
  }),
})
  .then((response) => response.json())
  .then((data) => console.log(data))

// 6
fetch('https://jsonplaceholder.typicode.com/todos/69', {
  method: 'DELETE',
})
  .then((response) => response.json())
  .then((data) => console.log(data))
