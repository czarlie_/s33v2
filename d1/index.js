// JavaScript Synchronous vs Asynchronous
// JavaScript is by default synchronous, meaning that one statement is executed one at a time
// console.log(`Hola Mundo! 🌍`)
// cosnole.log(`Hola Mundo! 🌍 Cosnole here! 👋`)
// console.log(`I'll run without any errors`)

// When certain statements take a lot of time to process, this slows down our code
// An example of this are when loops are used on a large amount of information or when fetching data from the databases

// for (let i = 0; i <= 15; i++) console.log(i)
// console.log(`Hola again!`) // only after the loop ends will this statement execute

// We might not notice due to the fast processing power of our devices

// Asynchronous means that we can proceed to execute other statements while time consuming code is running in the background

/*
  API stands for Application Programming Interface
    - is a particular set of codes that allows software programs to communicate with each other
    - simply put, an API is the interface through which you access someone's code or through which someone else's code accesses yours
    - allows to 'fetch' data from one application to another
*/

// Fetch API
// The fetch() method starts the process of fetching a resource from a server
// The fetch() method returns a PROMISE that is resolved to a Response Object

// The Fetch API allows you to asynchronously REQUEST for a resource (data)
// A 'promise' is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

// a promise is in one of these three states:
//  1) Pending:
//      - initial state, neither fulfilled nor rejected
//  2) Fulfilled:
//      - operation was successfully completed
// 3) Rejected:
//      - operation failed

// Syntax:
// fetch('URL')
// console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

/*
  By using '.then()' method => we can now check for the status of the promise
  'fetch()' method will return a PROMISE that resolves to be a response object
  '.then()' method will capture RESPONSE object and returns another promise which will eventually be 'resolved' or 'rejected'

  Syntax:
  fetch('URL')
  .then((response) => {someCodeLater})
*/

// fetch('https://jsonplaceholder.typicode.com/posts').then((response) =>
//   console.log(response.status)
// )

/*
  fetch function returns a promise. '.then' function allows for processing of that promise in order to extract data from it
  - it also waits for the promise to be fulfilled before moving forward

*/
// fetch('https://jsonplaceholder.typicode.com/posts')
// use the 'json()' method from the 'response' object to convert the data retrieved into JSON format to be used in our application
//   .then((response) => response.json())
// print the converted JSON value from the 'fetch request'
// using multiple 'then()' methods, creates a 'promise chain'
//   .then((data) => {
//     console.log(data)
//     console.log('')
//     console.log(`This will run after the promise has been fulfilled`)
//   })

// console.log(`This will run first`)

/*
  The 'async' and 'await' keywords
    - another approach that can be used to achieve asynchronous codes
    - used in functions to indicate which portions of codes should be waited
    - creates an aynchronous function 
*/

// async function fetchData() {
//   // waits for the fetch method to complete, then stores the value in the 'result' variable
//   let result = await fetch('https://jsonplaceholder.typicode.com/posts')
//   // result returned as a promise
//   console.log(result)
//   // the returned 'response' is an object
//   console.log(typeof result)

//   // we cannot access the content of the 'response' by directly accessing its body property
//   console.log(result.body)
//   let json = await result.json()
//   console.log(json)
// }

// fetchData()

// // Get a specific post
// // retrieves a specific post following the REST API (retrieve/get, /post/:id, GET)

// fetch('https://jsonplaceholder.typicode.com/posts/1')
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// // create a post
// // create a new post following the REST API (create/retrieve, /post/:id, POST)
// fetch('https://jsonplaceholder.typicode.com/posts', {
//   method: 'POST',
//   headers: { 'Content-Type': 'application/json' },
//   body: JSON.stringify({ title: 'New post', body: 'Hello World', userId: 1 }),
// })
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// // Updating a post
// // Update a specific post following the REST API (update, /posts/:id, POST)

// fetch('https://jsonplaceholder.typicode.com/posts/1', {
//   method: 'PUT',
//   headers: { 'Content-Type': 'application/json' },
//   body: JSON.stringify({
//     id: 1,
//     title: 'Updated post',
//     body: 'Hello Again',
//     userId: 1,
//   }),
// })
//   .then((response) => response.json())
//   .then((data) => console.log(data))

// // Deleting a post
// // Deleting a specific post following the REST API (delete, /posts/:id, DELETE)

// fetch('https://jsonplaceholder.typicode.com/posts/1', {
//   method: 'DELETE',
// })

// Mini-Activity #1
// Create a fetch request using the GET method that will retrieve all list items from JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => console.log(data))

// Mini-Activity #1
// Using the data retrieved, refactor and create an array using the map method to return just the title of every item and print the result in the console.
fetch('https://jsonplaceholder.typicode.com/todos')
  .then((response) => response.json())
  .then((data) => {
    let list = data.map((todo) => todo.title)
    console.log(list)
  })
